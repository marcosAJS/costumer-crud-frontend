import Customer from "../containers/customer/Customer";
import Login from "../components/Login";

const indexRoutes = [
    { path: "/", name: "Home", exact: true, component: Customer, protected: true }, 
    { path: "/home", name: "Home", exact: true, component: Customer, protected: true }, 
    { path: "/login", name: "Login", exact: true, component: Login, protected: false }, 
];

export default indexRoutes;