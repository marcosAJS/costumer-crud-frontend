import React, { Component } from 'react';
import Content from './Content';
import Sidebar from "./Sidebar";

class Layout extends Component {

    state = {
        isOpen: true
    }

    toggle = () => {
        this.setState({ isOpen: !this.state.isOpen });
    }

    render() {
        return (
            <div className="App wrapper">
                <Sidebar toggle={this.toggle} isOpen={this.state.isOpen} />
                <Content toggle={this.toggle} isOpen={this.state.isOpen} >
                    {this.props.children}
                </Content>
            </div>
        )
    }
}
export default Layout;