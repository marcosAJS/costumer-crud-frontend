
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';
import React from 'react';
import { NavLink } from "react-router-dom";
import { Nav, NavItem } from 'reactstrap';

const Sidebar = (props) => (
  <div className={classNames('sidebar', { 'is-open': props.isOpen })}>
    <div className="sidebar-header">
      {/* <a color="info" onClick={props.toggle} style={{color: '#fff'}}>&times;</a> */}
      <h3>Customer Manager</h3>
    </div>
    <Nav vertical className="list-unstyled pb-3">
      <NavItem>
        <NavLink to="/" ><FontAwesomeIcon icon={faUser} className="mr-2" />Clientes</NavLink>
      </NavItem>
    </Nav>
  </div>
)

export default Sidebar;