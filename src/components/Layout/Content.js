import React from 'react';
import { Container } from 'reactstrap';
import classNames from 'classnames';
import NavBar from './Navbar';

const Header = props => (
    <Container fluid className={classNames('header', { 'is-open': props.isOpen })}>
        <NavBar toggle={props.toggle} />
        {props.children}
    </Container>
);

export default Header;