import React, { Component } from 'react';
import { Button, Col, Container, Form, FormGroup, Input, Label } from 'reactstrap';
import { withRouter } from "react-router";

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: ""
        };
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = event => {
        event.preventDefault();
        this.props.history.push(`/home`)
    }

    render() {
        return (
            <Container className="Login">
                <h2>Sign In</h2>
                <Form onSubmit={this.handleSubmit} className="form">
                    <Col>
                        <FormGroup>
                            <Label>Email</Label>
                            <Input
                                type="text"
                                name="Username"
                                id="username"
                                placeholder="digite seu usuario"
                            />
                        </FormGroup>
                    </Col>
                    <Col>
                        <FormGroup>
                            <Label for="examplePassword">Password</Label>
                            <Input
                                type="password"
                                name="password"
                                id="password"
                                placeholder="********"
                            />
                        </FormGroup>
                    </Col>
                    <Button>Submit</Button>
                </Form>
            </Container>
        );
    }
}

export default withRouter(Login);