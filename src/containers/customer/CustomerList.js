import React from 'react';
import { Table, Button } from 'reactstrap';

const style = {
  btn: {
    marginLeft: "10px",
  }
}
const CustomerList = props => (
  <Table>
    <thead>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>CPF</th>
        <th>Cidade</th>
        <th>Estado</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      {[0, 1, 2, 3, 4, 5, 6].map(index => (
        <tr key={index}>
          <th scope="row">{index}</th>
          <td>Customer Name {index}</td>
          <td>026.447.841-07</td>
          <td>Brasilia</td>
          <td>DF</td>
          <td style={{ textAlign: 'right' }}>
            <Button style={style.btn} color="info">Detalhes</Button>
            <Button style={style.btn} color="primary">Detalhes</Button>
            <Button style={style.btn} color="danger">Excluir</Button>
          </td>
        </tr>
      ))}
    </tbody>
  </Table>
);

export default CustomerList;