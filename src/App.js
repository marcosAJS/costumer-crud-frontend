import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Layout from './components/Layout/Layout';
import { BrowserRouter, Route, Switch } from "react-router-dom"
import indexRoutes from './routes';
class App extends Component {
  render() {
    return (
      <BrowserRouter>

        <Switch>
          {indexRoutes.map((prop, key) => {
            return <Route exact path={prop.path} render={() => {
              if (prop.protected) {
                return <Layout> <prop.component /> </Layout>
              } else {
                return <prop.component />
              }
            }} key={key} />
          })}
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
